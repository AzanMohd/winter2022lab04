public class Laptop{
	private String brand;
	private int ram;
	private String os;
	
	public void brandIsAsus(){
		if(this.brand.equals("asus")){
			System.out.println("Good choice");
		}
		else{
			System.out.println("Bad choice");
		}
	}
	
	public String getBrand(){
		return this.brand;
	}
	public int getRam(){
		return this.ram;
	}
	public String getOs(){
		return this.os;
	}
	
	public void setBrand(String newBrand){
		this.brand = newBrand;
	}
	public void setRam(int newRam){
		this.ram = newRam;
	}
	/* public void setOs(String newOs){
		this.os = newOs;
	} */
	
	public Laptop(String brand, int ram, String os){
		this.brand = brand;
		this.ram = ram;
		this.os = os;
	}
}