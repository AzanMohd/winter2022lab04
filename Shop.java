import java.util.*;

public class Shop{
	public static void main(String[] args){
		Laptop[] laptop = new Laptop[4];
		Scanner sc= new Scanner(System.in);
		
		for(int i = 0; i < laptop.length; i++){
			
			System.out.println("Laptop "+(i+1));
			
			System.out.println("Enter a laptop brand:");
			String brand=sc.nextLine();
			System.out.println("Enter the ammount of ram:");
			int ram = Integer.parseInt(sc.nextLine());
			System.out.println("Enter the OS:");
			String os = sc.nextLine();
			System.out.println("-----------------------------");
			
			laptop[i] = new Laptop(brand, ram, os);
		} 
		laptop[3].setBrand("asus");
		laptop[3].setRam(15);
		
		System.out.println("Here is the last product you chose:");
		System.out.println(laptop[3].getBrand());
		System.out.println(laptop[3].getRam());
		System.out.println(laptop[3].getOs());
		laptop[3].brandIsAsus();
	}
}